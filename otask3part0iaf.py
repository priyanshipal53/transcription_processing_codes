import re
import io
def t3part0():
    from timeit import default_timer as timer
    start=timer()
    with open('test_shar_part0.txt','r', encoding="utf-8") as f:
        content_1 = f.read()
    content_1=re.sub(r'^\n','',content_1)
    content_1=re.sub(r'\n+','\n', content_1) #removing simulatenous new lines
    content_1=re.sub(r' *\n(?=,\D+)','.',content_1) #reformatting the lines
    file_pat=re.findall(r'file_\d\d\d\d.wav', content_1)
    print(len(file_pat))
    pattern=re.findall(r'file_\d\d\d\d.wav,.*\b(\?,|\?\.,|!\.,|\.,|\.\.,|\)\.,){1}.*(?:\?|\!|\.|\))*\n?', content_1)
    print(len(pattern))
    ##Verification
    for c in pattern: #double checking if all delimiters are expected
        assert c in ['?,','?.,','!.,','.,','..,',').,']
        #if c not in ['?,','?.,','!.,','.,','..,',').,']:
         #   print(c)
    count=0
    x=re.split(r'\n',content_1)
    if x[-1]=="\n":
        x.pop(-1)
    print("lines in the reformatted file=",len(x)) #should be equal to len(file_pat)
    assert len(file_pat)==len(x) #number of lines should be equal to number of files
    end=timer()
    print("--- %s seconds for t3part0---" , (end - start))
    return content_1
#with io.open('sharmistha-output-formatted.txt', 'w', encoding='utf-8') as f: #had an empty text file with this name in a directory already created
 #   f.write(content_1)





