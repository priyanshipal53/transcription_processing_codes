#!/usr/bin/env python
# coding: utf-8

import re
import docx2txt
import glob
import os

def combining_alldocx(x): #pass path or string
    from timeit import default_timer as timer
    start=timer()
    try:
        with open(x,'r', encoding="utf-8") as f:
            part=f.read()
    except:
        part=x
    f1=""
    #print("count of file no's in passed paramter=", len(re.findall(r'file_\d+.wav',part)))
    #with open("transcription wordfile1619-1719.txt",'r', encoding="utf-8") as f:
     #   f1 = f1+f.read()
    for file_name in glob.iglob(x+"/*.docx", recursive=True): #'C:\\Users\\priya\\Documents\\work\\prasanta\\work\\task 2\\CORRECTED SET 1 & 2-20200807T113500Z-001\\CORRECTED SET 1 _ 2/*.docx'
        #print(os.path.basename(file_name))
        f1=f1+docx2txt.process(os.path.basename(file_name))+"\n"
    #f1=f1+docx2txt.process("set1_2 not uploaded files.docx")+"\n"
    f1=re.sub(u'[\xc2\xa0\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200A\u2028\u2029\u202F\u205F\u3000]',' ', f1)
    f1=re.sub(r' +$','', f1, flags=re.MULTILINE) #replacing lines ending with spaces and new line with just a new line
    f1=re.sub(r'(?<=file_\d\d\d\d.wav)~','', f1, flags=re.M)
    f1=re.sub(r'(?<=file_\d\d\d\d.wav)\.','', f1, flags=re.M)
    files_count=re.findall(r'file_\d+.wav', f1)
    unique_files_count=set(re.findall(r'file_\d+.wav', f1))
    if len(files_count)!=len(unique_files_count):
           print("files are not unique in the set. Check duplicates") 
    assert len(files_count)==len(unique_files_count) #checking if all files are unique and if not, raise an exception
    print(len(re.findall(r'file_\d+.wav', f1)), len(set(re.findall(r'file_\d+.wav', f1)))) #just printing the numbers
    f1=re.sub(r' +$','', f1, flags=re.MULTILINE) #replacing lines ending with spaces and new line with just a new line
    formatted=""
    f1=re.sub(r'\n+','\n',f1, flags=re.M)
    if len(re.findall(r'>>',f1))>1: #assesing if it's the new format file (with ">>" as delimiter or the old ones
        f1=re.sub(r'\n\s*>>\s*',' >> ',f1, flags=re.M)
        file_nos=re.findall(r'file_\d+.wav',f1)
        if len(file_nos)!=len(re.findall(r'file_\d+.wav .*\s+>>.*',f1, flags=re.M)):
            for i in f1.splitlines():
                if re.search(r'file_\d+.wav .*\s+>>\s+.*',i)==None:
                    print("unmatching lines=",i)
        assert len(file_nos)==len(re.findall(r'file_\d+.wav .*\s+>>.*',f1, flags=re.M))
        return f1
        
    pattern=re.findall(r'\t?,?(file_\d\d\d\d.wav)\t?\n*(.*)\n+\t? *(.*)\n+', f1)
    for x,y,z in pattern:
        formatted+=x+" "+y+".,"+z+"\n"
    #combined=part+formatted
    combined=formatted
    pattern3=re.findall(r'(file_\d\d\d\d.wav) +(.*)\b(\?,|\?\.,|!\.,|\.,|\.\.\.,|\.\.,|\)\.,){1}(.*)(\?|\!|\.|\))*\n', combined)
    #pattern3=re.findall(r'(file_\d\d\d\d.wav) (.*)\b(\?,|\?\.,|!\.,|\.,|\.\.,|\)\.,){1}(.*)(\?|\!|\.|\))*\n', combined)
    print("length of lines fitting pattern in combined" ,len(pattern3))
    print("number of file no's in combined=",len(re.findall(r'file_\d+.wav', combined))) #should match with above pattern length.
    assert len(pattern3)==len(re.findall(r'file_\d+.wav', combined))
    end=timer()
    print("--- %s seconds for task3part1 glob one---", end-start)

    return combined
