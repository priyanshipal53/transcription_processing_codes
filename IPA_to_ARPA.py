#!/usr/bin/env python
# coding: utf-8

# In[34]:

import re
import io
def IPA_to_ARPA(file): 
    try:
        with open(file,'r', encoding="utf-8") as f:
            contents = f.read()
    except:
        contents=file
    with open('forPhoneMapping.txt','r', encoding="utf-8") as f:
        phone_content_f = f.read()
    with open('timitIPAtoArpa.txt','r', encoding="utf-8") as f2:
        timit_content_f = f2.read()
    phone_map_count=0
    total_dict={}
    timit_map_count=0
    phonemap=re.findall(r'(\S+)\t.*\t(.*)', phone_content_f)
    for i,y in phonemap:
        total_dict[i]=y
        phone_map_count+=1
    timitmap=re.findall(r'(\S+)\t(\S+)', timit_content_f)
    for j,k in timitmap:
        total_dict[j]=k.upper()
        timit_map_count+=1
    print("phonemes in forPhoneMaping=",(phone_map_count), ", phonemes in timitIPAtoArpa=",(timit_map_count), ", total unique symbols in both=",len(total_dict))
    pattern=re.findall(r'.*(file_\d\d\d\d.wav) (.*) >> (.*)', contents) #(\?|\!|\.|\))*
    print("count of lines fitting in pattern=",len(pattern))
    for i in contents.splitlines():
        if re.search(r'.*file_\d\d\d\d.wav .* >> .*',i)==None:
            print(i)
    converted_str=""
    converted_str_nosentence=""
    invalid_chars=[]
    for x,z,p in pattern:
        mat_=re.findall(r'[^\s~]{3,}',p)
        mat1_=re.findall(r'[^\s~]{2}',p)#(r'\S{2,3}') #match symbols with two or 3 characters like i: / t̪ / d̪h (counted as char length=3)
        mat2_=re.findall(r'[^\s~]',p)#(r'\S{1}') #match single character like ɽ or ɖ etc
        for j in mat_: #matches for len(char)=3
            if j in total_dict:
                p=p.replace(j, total_dict[j])
        for m in mat1_: ##matches for len(char)=2
            if m in total_dict:
                p=p.replace(m, total_dict[m])
        for l in mat2_: #matches for len(char)=1
            if l in total_dict:
                p=p.replace(l, total_dict[l])
            #print(i)
        p=re.sub(r' +',' ',p)
        p=re.sub(r'\t+',' ',p)
        #verification-1 :to ensure if all got converted, checking if any phonetic symbol doesn't match the arpa symbols (dictionary values) 
        check_pat=re.findall(r'[^\s~]+',p)
        invalid_chars.append([t for t in check_pat if t not in total_dict.values()]) 
        #verification-2 :If anything is left unconverted, it will appear in lower case
        for j in check_pat:
            if j.islower()==True:
                print(i,"unconverted",x,z,p)
        converted_str+=x+" "+z+" >> "+p.lower()+"\n" #processed string from IPA to ARPA
        converted_str_nosentence+=x+" "+p.lower()+"\n"
    print("number of arpa symbols not in forPhoneMapping or timitIPAtoArpa=",len(list(filter(None, invalid_chars)))) #len should be 0- output of verification method 1
    assert len(list(filter(None, invalid_chars)))==0
   # with io.open('IPAtoARPA_trans_from_linguists_latest.txt', 'w', encoding='utf-8') as f:
    #    f.write(converted_str)
    return converted_str,converted_str_nosentence





