import re
import io

def submap(file,file2):
    try:
        with open(file,'r', encoding="utf-8") as f:
            contents = f.read()
    except:
        contents=file
    with open(file2,'r', encoding="utf-8") as f: #'subMapInfo_3034.txt'
        submapinfo = f.read()
    submapdict={}
    submap=re.findall(r'(\w+ *\w+_.*) \t?(file_\d+\.wav)',submapinfo)
    for a,b in submap:
        a=re.sub(r' +','_',a)
        submapdict[b]=a
    line_pattern=re.findall(r'(file_\d+.wav)(?: .* >> )*( *.*)', contents, re.MULTILINE)
    converted_string=""
    converted_string_wfn=""
    #converted_string_wfn2=""
    for k,(z,i) in enumerate(line_pattern):
        converted_string+=submapdict[z]+" "+i+"\n"
    converted_string=re.sub(r' +',' ', converted_string)
    return converted_string



#p6out=part6("IPAtoArpa_converted_set_1_2.txt","subMapInfo.txt")


#import io
#with io.open('aaaaaaaa.txt', 'w', encoding='utf-8') as f:
 #   f.write(p6out)

