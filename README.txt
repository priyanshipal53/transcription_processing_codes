To process transcription from the stage of 
combining them -> finding unique phoneme symbols-> finding symbols which are not valid-> replacing invalid to valid symbols-> correcting any inconsistent instances where number of terms in a sentence don't match the terms in phonetic transcription->
Converting IPA symbols into ARPA format-> Mapping ARPA symbols to a set of 40 phonemes-> mapping file numbers to their speaker+label information (submapping)


The codes are to be executed in the following manner:

1. combining_files
2. p2new 
3. invalid_symbols
4. p4p4_withsentence
5. Unequal_terms_fix 
6. IPA_to_ARPA
7. arpa_40phonemes
8. submapping

Refer to the ipynb notebook where they are all executed accordingly.

The required paths are to be put in the codes which require them and it is to be made sure that the files (transcriptions) are in the same working directory as the codes.
#some essential files used: forPhoneMapping.txt, timitIPAtoArpa.txt, myphones.64-48-40.txt, respective submapping file for a set, approximate_mapping.txt
